import { Component } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
@Component({
    selector: 'app-tab2',
    templateUrl: 'tab2.page.html',
    styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

    constructor(
        private localNotifications: LocalNotifications,
        private nativeAudio: NativeAudio
    ) { }


    click() {
        this.localNotifications.schedule({
            id: 1,
            title: 'Arrival Notification',
            text: 'You have reached.',
            smallIcon: 'res://icon',
            icon: 'file://assets/icon/icon.png',
            sound: 'res://sound.wav',
            trigger: {
                at: new Date(new Date().getTime() + 6 * 1000),
            }
        });
    }



    // // Schedule multiple notifications
    // this.localNotifications.schedule([{
    //     id: 1,
    //     text: 'Multi ILocalNotification 1',
    //     sound: isAndroid ? 'file://sound.mp3' : 'file://beep.caf',
    //     data: { secret: key }
    // }, {
    //     id: 2,
    //     title: 'Local ILocalNotification Example',
    //     text: 'Multi ILocalNotification 2',
    //     icon: 'http://example.com/icon.png'
    // }]);


    // // Schedule delayed notification
    // this.localNotifications.schedule({
    //     text: 'Delayed ILocalNotification',
    //     trigger: { at: new Date(new Date().getTime() + 3600) },
    //     led: 'FF0000',
    //     sound: null
    // });

}
